<?php

namespace App\Http\Controllers;

use App\Models\Perfil;
use Illuminate\Http\Request;

class PerfilController extends Controller
{
    public function index()
    {
        $perfil = Perfil::first();
        
        return view('frontend.perfil', compact('perfil'));
    }

    public function getDados()
    {
        $perfil = Perfil::first();

        return response()->json(['perfil' => $perfil]);
    }
}