<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\BannersRequest;
use App\Models\Banner;
use Illuminate\Http\Request;

class BannersController extends Controller
{
    public function index()
    {
        $banners = Banner::ordenados()->get();

        return view('painel.banners.index', compact('banners'));
    }

    public function create()
    {
        return view('painel.banners.create');
    }

    public function store(BannersRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Banner::upload_imagem();

            Banner::create($input);

            return redirect()->route('banners.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Banner $banner)
    {
        return view('painel.banners.edit', compact('banner'));
    }

    public function update(BannersRequest $request, Banner $banner)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Banner::upload_imagem();

            $banner->update($input);

            return redirect()->route('banners.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Banner $banner)
    {
        try {
            $banner->delete();

            return redirect()->route('banners.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
