<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClippingsRequest;
use App\Models\Clipping;
use App\Models\Tipo;
use Illuminate\Http\Request;

class ClippingsController extends Controller
{
    private function removerAcentos()
    {
        $conversao = array(
            'á' => 'a', 'à' => 'a', 'ã' => 'a', 'â' => 'a', 'é' => 'e',
            'ê' => 'e', 'í' => 'i', 'ï' => 'i', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', "ö" => "o",
            'ú' => 'u', 'ü' => 'u', 'ç' => 'c', 'ñ' => 'n', 'Á' => 'A', 'À' => 'A', 'Ã' => 'A',
            'Â' => 'A', 'É' => 'E', 'Ê' => 'E', 'Í' => 'I', 'Ï' => 'I', "Ö" => "O", 'Ó' => 'O',
            'Ô' => 'O', 'Õ' => 'O', 'Ú' => 'U', 'Ü' => 'U', 'Ç' => 'C', 'Ñ' => 'N'
        );

        return $conversao;
    }

    public function index()
    {
        $tipos = Tipo::orderBy('id', 'asc')->get();

        if (isset($_GET['tipo'])) {
            $tipo = $_GET['tipo'];
            $clippings = Clipping::tipo($tipo)
                ->join('tipos', 'tipos.id', '=', 'clippings.tipo_id')
                ->select('tipos.titulo as tipo', 'clippings.*')
                ->ordenados()->get();
        } else {
            $clippings = Clipping::join('tipos', 'tipos.id', '=', 'clippings.tipo_id')
                ->select('tipos.titulo as tipo', 'clippings.*')->ordenados()->get();
        }

        return view('painel.clippings.index', compact('clippings', 'tipos'));
    }

    public function create()
    {
        $tipos = Tipo::orderBy('id', 'asc')->pluck('titulo', 'id');

        return view('painel.clippings.create', compact('tipos'));
    }

    public function store(ClippingsRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Clipping::upload_capa();

            $file = $request->file('arquivo');

            if ($request->hasFile('arquivo')) {
                if ($request->file('arquivo')->isValid()) {
                    $filename = strtoupper(str_replace(' ', '-', strtr($file->getClientOriginalName(), $this->removerAcentos())));
                    $path = public_path() . '/assets/arquivos/clipping';
                    $file->move($path, $filename);
                    $input['arquivo'] = $filename;
                }
            }

            Clipping::create($input);

            return redirect()->route('clippings.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Clipping $clipping)
    {
        $tipos = Tipo::orderBy('id', 'asc')->pluck('titulo', 'id');

        return view('painel.clippings.edit', compact('clipping', 'tipos'));
    }

    public function update(ClippingsRequest $request, Clipping $clipping)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Clipping::upload_capa();

            $file = $request->file('arquivo');

            if ($request->hasFile('arquivo')) {
                if ($request->file('arquivo')->isValid()) {
                    $filename = strtoupper(str_replace(' ', '-', strtr($file->getClientOriginalName(), $this->removerAcentos())));
                    $path = public_path() . '/assets/arquivos/clipping';
                    $file->move($path, $filename);
                    $input['arquivo'] = $filename;
                }
            }

            $clipping->update($input);

            return redirect()->route('clippings.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Clipping $clipping)
    {
        try {
            $clipping->delete();

            return redirect()->route('clippings.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
