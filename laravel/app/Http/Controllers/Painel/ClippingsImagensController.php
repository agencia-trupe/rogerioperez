<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClippingsImagensRequest;
use App\Models\Clipping;
use App\Models\ClippingImagem;
use Illuminate\Http\Request;

class ClippingsImagensController extends Controller
{
    public function index(Clipping $clipping)
    {
        $imagens = ClippingImagem::clipping($clipping->id)->ordenados()->get();

        return view('painel.clippings.imagens.index', compact('clipping', 'imagens'));
    }

    public function show(Clipping $clipping, ClippingImagem $imagen)
    {
        $imagem = $imagen;
        return $imagem;
    }

    public function store(ClippingsImagensRequest $request, Clipping $clipping)
    {
        try {
            $input = $request->all();
            $input['imagem'] = ClippingImagem::upload_imagem();
            $input['clipping_id'] = $clipping->id;

            $imagem = ClippingImagem::create($input);

            $view = view('painel.clippings.imagens.imagem', compact('clipping', 'imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Clipping $clipping, ClippingImagem $imagen)
    {
        try {
            $imagen->delete();

            return redirect()->route('clippings.imagens.index', $clipping->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }

    public function clear(Clipping $clipping)
    {
        try {
            $clipping->imagens()->delete();

            return redirect()->route('clippings.imagens.index', $clipping->id)->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
