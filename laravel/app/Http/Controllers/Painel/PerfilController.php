<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\PerfilRequest;
use App\Models\Perfil;
use Illuminate\Http\Request;

class PerfilController extends Controller
{
    public function index()
    {
        $perfil = Perfil::first();

        return view('painel.perfil.edit', compact('perfil'));
    }

    public function update(PerfilRequest $request, Perfil $perfil)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Perfil::upload_imagem();

            $perfil->update($input);

            return redirect()->route('perfil.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
