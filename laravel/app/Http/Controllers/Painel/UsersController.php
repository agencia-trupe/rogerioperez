<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\UsersRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    public function index()
    {
        $usuarios = User::all();

        return view('painel.usuarios.index', compact('usuarios'));
    }

    public function create()
    {
        return view('painel.usuarios.create');
    }

    public function store(UsersRequest $request)
    {
        try {
            $input = $request->all();
            $input['password'] = bcrypt($input['password']);

            User::create($input);

            return redirect()->route('usuarios.index')->with('success', 'Usuário adicionado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao adicionar usuário: ' . $e->getMessage()]);
        }
    }

    public function edit(User $usuario)
    {
        return view('painel.usuarios.edit', compact('usuario'));
    }

    public function update(Request $request, User $usuario)
    {
        try {
            $rules = array(
                'name'     => 'max:255',
                'password' => 'confirmed|min:8',
            );
            $messages = array(
                'confirmed' => 'A confirmação de senha não confere. Sua senha deve ter no mínimo 8 caracteres.',
            );
            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $input = $request->all();
            if (isset($input['password'])) $input['password'] = bcrypt($input['password']);

            $usuario->update($input);

            return redirect()->route('usuarios.index')->with('success', 'Usuário alterado com sucesso.');
        } catch (\Throwable $e) {
            return back()->withErrors(['Erro ao alterar usuário: ' . $e->getMessage()]);
        }
    }

    public function destroy(User $usuario)
    {
        try {
            $usuario->delete();

            return redirect()->route('usuarios.index')->with('success', 'Usuário excluído com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao excluir usuário: ' . $e->getMessage()]);
        }
    }
}
