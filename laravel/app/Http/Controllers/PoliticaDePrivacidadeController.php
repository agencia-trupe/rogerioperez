<?php

namespace App\Http\Controllers;

use App\Models\PoliticaDePrivacidade;
use Illuminate\Http\Request;

class PoliticaDePrivacidadeController extends Controller
{
    public function index()
    {
        $politica = PoliticaDePrivacidade::first();

        return view('frontend.politica-de-privacidade', compact('politica'));
    }

    public function getDados()
    {
        $politica = PoliticaDePrivacidade::first();

        return response()->json(['politica' => $politica]);
    }
}
