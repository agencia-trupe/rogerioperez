<?php

namespace App\Http\Controllers;

use App\Models\Clipping;
use App\Models\ClippingImagem;
use Illuminate\Http\Request;

class ClippingController extends Controller
{
    public function index()
    {
        $clippings = Clipping::ordenados()->get();

        return view('frontend.clipping', compact('clippings'));
    }

    public function getDados()
    {
        $clippings = Clipping::ordenados()->get();

        return response()->json(['clippings' => $clippings]);
    }

    public function show($id)
    {
        $imagens = ClippingImagem::where('clipping_id', $id)->ordenados()->get();

        return view('frontend.clipping-show', compact('imagens'));
    }

    public function getShowDados($id)
    {
        $imagens = ClippingImagem::where('clipping_id', $id)->ordenados()->get();

        return response()->json(['imagens' => $imagens, 'id' => $id]);
    }

    public function showPdf($id)
    {
        $arquivo = Clipping::where('id', $id)->select('arquivo')->first();

        $arquivoPath = public_path() . "/assets/arquivos/clipping/" . $arquivo->arquivo;

        return response()->file($arquivoPath);
    }
}
