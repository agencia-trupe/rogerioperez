<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PerfilRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'imagem'   => 'image',
            'frase_pt' => 'required',
            'frase_en' => '',
            'frase_es' => '',
            'texto_pt' => 'required',
            'texto_en' => '',
            'texto_es' => '',
        ];
    }
}
