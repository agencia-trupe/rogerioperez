<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    use HasFactory;

    protected $table = 'perfil';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('imagem', [
                'width'  => 500,
                'height' => null,
                'path'   => 'assets/img/perfil/'
            ]);
        } else {
            return CropImage::make('imagem', [
                'width'  => 500,
                'height' => null,
                'path'   => 'assets/img/perfil/'
            ]);
        }
    }
}
