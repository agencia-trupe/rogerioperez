<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjetoImagem extends Model
{
    use HasFactory;

    protected $table = 'projetos_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeProjeto($query, $id)
    {
        return $query->where('projeto_id', $id);
    }

    public static function upload_imagem()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('imagem', [
                [
                    'width'  => null,
                    'height' => 650,
                    'upsize'  => true,
                    'path'    => 'assets/img/projetos/imagens/'
                ],
                [
                    'width'  => 1500,
                    'height' => null,
                    'path'    => 'assets/img/projetos/imagens/fancybox/'
                ]
            ]);
        } else {
            return CropImage::make('imagem', [
                [
                    'width'  => null,
                    'height' => 650,
                    'upsize'  => true,
                    'path'    => 'assets/img/projetos/imagens/'
                ],
                [
                    'width'  => 1500,
                    'height' => null,
                    'path'    => 'assets/img/projetos/imagens/fancybox/'
                ]
            ]);
        }
    }
}
