<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClippingImagem extends Model
{
    use HasFactory;

    protected $table = 'clippings_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeClipping($query, $id)
    {
        return $query->where('clipping_id', $id);
    }

    public static function upload_imagem()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('imagem', [
                'width'  => null,
                'height' => 800,
                'upsize'  => true,
                'path'    => 'assets/img/clippings/imagens/'
            ]);
        } else {
            return CropImage::make('imagem', [
                'width'  => null,
                'height' => 800,
                'upsize'  => true,
                'path'    => 'assets/img/clippings/imagens/'
            ]);
        }
    }
}
