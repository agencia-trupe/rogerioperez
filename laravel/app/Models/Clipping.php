<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clipping extends Model
{
    use HasFactory;

    protected $table = 'clippings';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeTipo($query, $id)
    {
        return $query->where('tipo_id', $id);
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\ClippingImagem', 'clipping_id')->ordenados();
    }

    public static function upload_capa()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('capa', [
                'width'  => 310,
                'height' => null,
                'path'   => 'assets/img/clippings/'
            ]);
        } else {
            return CropImage::make('capa', [
                'width'  => 310,
                'height' => null,
                'path'   => 'assets/img/clippings/'
            ]);
        }
    }
}
