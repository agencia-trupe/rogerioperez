<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\Sluggable;

class Projeto extends Model
{
    use HasFactory;

    protected $table = 'projetos';

    protected $guarded = ['id'];

    use Sluggable;

    public function sluggable(): array
    {
        return [
            'slug_pt' => [
                'source' => 'titulo_pt'
            ],
            'slug_en' => [
                'source' => 'titulo_en'
            ],
            'slug_es' => [
                'source' => 'titulo_es'
            ]
        ];
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\ProjetoImagem', 'projeto_id')->ordenados();
    }

    public static function upload_capa()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('capa', [
                'width'  => 310,
                'height' => 250,
                'path'   => 'assets/img/projetos/'
            ]);
        } else {
            return CropImage::make('capa', [
                'width'  => 310,
                'height' => 250,
                'path'   => 'assets/img/projetos/'
            ]);
        }
    }
}
