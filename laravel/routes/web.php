<?php

use App\Http\Controllers\ClippingController;
use App\Http\Controllers\ContatoController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PerfilController;
use App\Http\Controllers\PoliticaDePrivacidadeController;
use App\Http\Controllers\ProjetosController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('perfil', [PerfilController::class, 'index'])->name('perfil'); 
Route::get('perfil/ajax', [PerfilController::class, 'getDados'])->name('perfil.ajax');

Route::get('projetos', [ProjetosController::class, 'index'])->name('projetos');
Route::get('projetos/ajax', [ProjetosController::class, 'getDados'])->name('projetos.ajax');

Route::get('projetos/{slug}', [ProjetosController::class, 'show'])->name('projetos-show');
Route::get('projetos/{slug}/ajax', [ProjetosController::class, 'getShowDados'])->name('projetos-show.ajax');
Route::get('projetos/{slug}/proximo', [ProjetosController::class, 'getShowDadosProximo'])->name('projetos-proximo.ajax');
Route::get('projetos/{slug}/anterior', [ProjetosController::class, 'getShowDadosAnterior'])->name('projetos-anterior.ajax');

Route::get('clipping', [ClippingController::class, 'index'])->name('clipping');
Route::get('clipping/ajax', [ClippingController::class, 'getDados'])->name('clipping.ajax');
Route::get('clipping/{id}', [ClippingController::class, 'show'])->name('clipping.show');
Route::get('clipping/ajax/{id}', [ClippingController::class, 'getShowDados'])->name('clipping-show.ajax');
Route::get('clipping/{id}/arquivo', [ClippingController::class, 'showPdf'])->name('clipping.arquivo');

Route::get('contato', [ContatoController::class, 'index'])->name('contato');
Route::get('contato/ajax', [ContatoController::class, 'getDados'])->name('contato.ajax');
Route::post('contato', [ContatoController::class, 'post'])->name('contato.post');

Route::get('politica-de-privacidade', [PoliticaDePrivacidadeController::class, 'index'])->name('politica-de-privacidade');
Route::get('politica-de-privacidade/ajax', [PoliticaDePrivacidadeController::class, 'getDados'])->name('politica-de-privacidade.ajax');

Route::post('aceite-de-cookies', [HomeController::class, 'postCookies'])->name('aceite-de-cookies.post');

// LANG
Route::get('lang/{lang}', function ($lang) {
    if (in_array($lang, ['pt', 'en', 'es'])) {
        Session::put('locale', $lang);
    }
    return back();
})->name('lang');


require __DIR__ . '/auth.php';
