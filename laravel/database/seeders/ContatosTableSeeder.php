<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContatosTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contatos')->insert([
            'email'           => 'contato@trupe.net',
            'telefone'        => '+55 11 5090 0423',
            'celular'         => '+55 11 98370 0547',
            'instagram'       => 'https://www.instagram.com/rogerioperezarquitetura/',
            'endereco_pt'     => 'Alameda dos Jurupis 452 ∙ 9° andar ∙ bloco B',
            'bairro_pt'       => 'Moema',
            'cidade_pt'       => 'São Paulo, SP',
            'pais_pt'         => 'Brasil',
            'cep'             => '04088-001',
            'google_maps'     => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.0278933051222!2d-46.66146438440593!3d-23.60333256903007!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5a1a9267eea1%3A0x79160b9c441a7b5e!2sAlameda%20dos%20Jurupis%2C%20452%20-%20b%20-%20Indian%C3%B3polis%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2004088-001!5e0!3m2!1spt-BR!2sbr!4v1638801517441!5m2!1spt-BR!2sbr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>',
        ]);
    }
}
