<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PerfilTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('perfil')->insert([
            'imagem'   => '',
            'frase_pt' => 'Completo e holístico: tudo sob uma mesma visão',
            'frase_en' => '',
            'frase_es' => '',
            'texto_pt' => '',
            'texto_en' => '',
            'texto_es' => '',
        ]);
    }
}
