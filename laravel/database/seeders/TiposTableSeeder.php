<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TiposTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('tipos')->insert([
            'id'     => 1,
            'titulo' => 'Imagens',
        ]);

        DB::table('tipos')->insert([
            'id'     => 2,
            'titulo' => 'Link',
        ]);

        DB::table('tipos')->insert([
            'id'     => 3,
            'titulo' => 'Vídeo',
        ]);

        DB::table('tipos')->insert([
            'id'     => 4,
            'titulo' => 'PDF',
        ]);
    }
}
