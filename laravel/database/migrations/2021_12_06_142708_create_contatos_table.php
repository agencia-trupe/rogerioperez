<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContatosTable extends Migration
{
    public function up()
    {
        Schema::create('contatos', function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->string('telefone');
            $table->string('celular');
            $table->string('instagram');
            $table->string('endereco_pt');
            $table->string('endereco_en')->nullable();
            $table->string('endereco_es')->nullable();
            $table->string('bairro_pt');
            $table->string('bairro_en')->nullable();
            $table->string('bairro_es')->nullable();
            $table->string('cidade_pt');
            $table->string('cidade_en')->nullable();
            $table->string('cidade_es')->nullable();
            $table->string('pais_pt');
            $table->string('pais_en')->nullable();
            $table->string('pais_es')->nullable();
            $table->string('cep');
            $table->text('google_maps');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('contatos');
    }
}
