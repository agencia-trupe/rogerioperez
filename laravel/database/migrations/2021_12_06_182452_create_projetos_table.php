<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjetosTable extends Migration
{
    public function up()
    {
        Schema::create('projetos', function (Blueprint $table) {
            $table->id();
            $table->integer('ordem')->default(0);
            $table->string('capa');
            $table->string('slug_pt');
            $table->string('slug_en')->nullable();
            $table->string('slug_es')->nullable();
            $table->string('titulo_pt');
            $table->string('titulo_en')->nullable();
            $table->string('titulo_es')->nullable();
            $table->string('localizacao_pt');
            $table->string('localizacao_en')->nullable();
            $table->string('localizacao_es')->nullable();
            $table->text('descritivo_pt');
            $table->text('descritivo_en')->nullable();
            $table->text('descritivo_es')->nullable();
            $table->string('cor_texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('projetos');
    }
}
