<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClippingsImagensTable extends Migration
{
    public function up()
    {
        Schema::create('clippings_imagens', function (Blueprint $table) {
            $table->id();
            $table->foreignId('clipping_id')->constrained('clippings')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('clippings_imagens');
    }
}
