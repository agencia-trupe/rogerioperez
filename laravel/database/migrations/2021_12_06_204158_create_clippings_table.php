<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClippingsTable extends Migration
{
    public function up()
    {
        Schema::create('clippings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tipo_id')->constrained('tipos')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('capa')->nullable();
            $table->string('link')->nullable();
            $table->string('video')->nullable();
            $table->string('arquivo')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('clippings');
    }
}
