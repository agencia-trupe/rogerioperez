<?php

return [
    'slug'        => 'slug_en',
    'titulo'      => 'titulo_en',
    'frase'       => 'frase_en',
    'texto'       => 'texto_en',
    'descritivo'  => 'descritivo_en',
    'endereco'    => 'endereco_en',
    'bairro'      => 'bairro_en',
    'cidade'      => 'cidade_en',
    'pais'        => 'pais_en',
    'localizacao' => 'localizacao_en',
];
