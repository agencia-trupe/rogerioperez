<?php

return [
    'geral' => [
        'politica' => 'PRIVACY POLICY',
        'perfil'   => 'PROFILE',
        'projetos' => 'PROJECTS',
        'clipping' => 'CLIPPING',
        'contato'  => 'CONTACT',
    ],

    'projetos' => [
        'projeto'     => 'PROJECT',
        'localizacao' => 'LOCALIZATION',
        'descritivo'  => 'DESCRIPTIVE',
        'topo'        => 'top',
        'proximo'     => 'next project',
        'anterior'    => 'previous project',
    ],

    'clipping' => [
        'ver-mais' => 'see more +',
        'voltar'   => '« return',
    ],

    'contato' => [
        'nome'        => 'name',
        'telefone'    => 'telephone',
        'mensagem'    => 'message',
        'enviar'      => 'TO SEND',
        'msg-sucesso' => 'Message sent successfully!',
    ],

    'direitos'    => 'All rights reserved.',
    'criacao'     => 'Website creation:',
    'cookies1'    => 'We use cookies to personalize content, track ads and provide you with a safer browsing experience. By continuing to browse our site, you agree to our use of this information. Read our ',
    'cookies2'    => 'Privacy Policy',
    'cookies3'    => ' and learn more.',
    'btn-cookies' => 'ACCEPT AND CLOSE',
];