<?php

return [
    'slug'        => 'slug_pt',
    'titulo'      => 'titulo_pt',
    'frase'       => 'frase_pt',
    'texto'       => 'texto_pt',
    'descritivo'  => 'descritivo_pt',
    'endereco'    => 'endereco_pt',
    'bairro'      => 'bairro_pt',
    'cidade'      => 'cidade_pt',
    'pais'        => 'pais_pt',
    'localizacao' => 'localizacao_pt',
];
