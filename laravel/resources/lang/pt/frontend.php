<?php

return [
    'geral' => [
        'politica' => 'POLÍTICA DE PRIVACIDADE',
        'perfil'   => 'PERFIL',
        'projetos' => 'PROJETOS',
        'clipping' => 'CLIPPING',
        'contato'  => 'CONTATO',
    ],

    'projetos' => [
        'projeto'     => 'PROJETO',
        'localizacao' => 'LOCALIZAÇÃO',
        'descritivo'  => 'DESCRITIVO',
        'topo'        => 'topo',
        'proximo'     => 'próximo projeto',
        'anterior'    => 'projeto anterior',
    ],

    'clipping' => [
        'ver-mais' => 'ver mais +',
        'voltar'   => '« voltar',
    ],

    'contato' => [
        'nome'        => 'nome',
        'telefone'    => 'telefone',
        'mensagem'    => 'mensagem',
        'enviar'      => 'ENVIAR',
        'msg-sucesso' => 'Mensagem enviada com sucesso!',
    ],

    'direitos'    => 'Todos os direitos reservados.',
    'criacao'     => 'Criação de sites:',
    'cookies1'    => 'Usamos cookies para personalizar o conteúdo, acompanhar anúncios e oferecer uma experiência de navegação mais segura a você. Ao continuar navegando em nosso site você concorda com o uso dessas informações. Leia nossa ',
    'cookies2'    => 'Política de Privacidade',
    'cookies3'    => ' e saiba mais.',
    'btn-cookies' => 'ACEITAR E FECHAR',
];