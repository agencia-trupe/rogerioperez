<?php

return [
    'geral' => [
        'politica' => 'POLÍTICA DE PRIVACIDAD',
        'perfil'   => 'PERFIL',
        'projetos' => 'PROYECTOS',
        'clipping' => 'RECORTE',
        'contato'  => 'CONTACTO',
    ],

    'projetos' => [
        'projeto'     => 'PROYECTO',
        'localizacao' => 'LOCALIZACIÓN',
        'descritivo'  => 'DESCRIPTIVO',
        'topo'        => 'cima',
        'proximo'     => 'próximo proyecto',
        'anterior'    => 'proyecto anterior',
    ],

    'clipping' => [
        'ver-mais' => 'ver más +',
        'voltar'   => '« regreso',
    ],

    'contato' => [
        'nome'        => 'nombre',
        'telefone'    => 'teléfono',
        'mensagem'    => 'mensaje',
        'enviar'      => 'MANDAR',
        'msg-sucesso' => 'Mensaje enviado correctamente!',
    ],

    'direitos'    => 'Todos los derechos reservados.',
    'criacao'     => 'Creación de sitio web:',
    'cookies1'    => 'Usamos cookies para personalizar contenido, rastrear anuncios y brindarle una experiencia de navegación más segura. Si continúa navegando en nuestro sitio, acepta nuestro uso de esta información. Lea nuestra ',
    'cookies2'    => 'Política de Privacidad',
    'cookies3'    => ' y obtenga más información.',
    'btn-cookies' => 'ACEPTAR Y CERRAR',
];