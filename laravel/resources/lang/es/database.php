<?php

return [
    'slug'        => 'slug_es',
    'titulo'      => 'titulo_es',
    'frase'       => 'frase_es',
    'texto'       => 'texto_es',
    'descritivo'  => 'descritivo_es',
    'endereco'    => 'endereco_es',
    'bairro'      => 'bairro_es',
    'cidade'      => 'cidade_es',
    'pais'        => 'pais_es',
    'localizacao' => 'localizacao_es',
];
