export default function AjaxPerfil() {
    $(document).ready(function () {
        var urlPrevia = "";
        // var urlPrevia = "/previa-rogerioperez";

        // PERFIL - ajax
        $("body").on("click", ".link-perfil", function (e) {
            e.preventDefault();
            $(".link-nav.active, .link-footer.active, .link-politica.active").removeClass("active");
            $(".link-perfil").addClass("active");
            getDadosPerfil(this.href);
        });

        var getDadosPerfil = function (url) {
            $("main").animate({'opacity':'0.0'}, 800, function () {
                $.ajax({
                    type: "GET",
                    url: url,
                    cache: false,
                    beforeSend: function () {
                        $("main").html("");
                        $("main").removeClass().addClass("perfil");
                    },
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);

                        var html =
                            '<div class="center"><article class="left"><img src="' +
                            window.location.origin +urlPrevia+
                            "/assets/img/perfil/" +
                            data.perfil.imagem +
                            '" class="img-perfil"><div class="frase frase-perfil"></div></article><article class="right texto-perfil"></article></div>';

                        $("main.perfil").append(html);

                        if (langActive == "en") {
                            var frase = data.perfil.frase_en;
                            var texto = data.perfil.texto_en;
                        } else if (langActive == "es") {
                            var frase = data.perfil.frase_es;
                            var texto = data.perfil.texto_es;
                        } else {
                            var frase = data.perfil.frase_pt;
                            var texto = data.perfil.texto_pt;
                        }
                        $(".center .frase-perfil").append(frase);
                        $(".center .texto-perfil").append(texto);

                        $("main").animate({'opacity':'1.0'}, 800);

                        window.history.pushState("", "", urlPrevia+"/perfil");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR, textStatus, errorThrown);
                    },
                });
            });
        };
    });
}
