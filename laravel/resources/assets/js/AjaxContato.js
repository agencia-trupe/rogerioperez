export default function AjaxContato() {
    $(document).ready(function () {
        var urlPrevia = "";
        // var urlPrevia = "/previa-rogerioperez";

        // CONTATO - ajax
        $("body").on("click", ".link-contato", function (e) {
            e.preventDefault();
            $(".link-nav.active, .link-footer.active, .link-politica.active").removeClass("active");
            $(".link-contato").addClass("active");
            getDadosContato(this.href);
        });

        var getDadosContato = function (url) {
            $("main").animate({ opacity: "0.0" }, 800, function () {
                $.ajax({
                    type: "GET",
                    url: url,
                    cache: false,
                    beforeSend: function () {
                        $("main").html("");
                        $("main").removeClass().addClass("contato");
                    },
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);

                        var baseHtml =
                            '<div class="center"><section class="dados-form"></section><section class="endereco-mapa"></section></div>';
                        $("main.contato").append(baseHtml);

                        var htmlDados = '<article class="dados"><a href="tel:'+data.contato.telefone.replace(" ", "")+'" class="link-telefone">'+data.contato.telefone+'</a><a href="https://api.whatsapp.com/send?phone='+data.contato.celular.replace(" ", "")+'" class="link-wpp" target="_blank">'+data.contato.celular+'</a><a href="'+data.contato.instagram+'" target="_blank" class="link-instagram">@rogerioperezarquitetura</a></article>';
                        $("main.contato section.dados-form").append(htmlDados);

                        var htmlForm = '<form action="'+window.location.origin+urlPrevia+'/contato'+'" method="POST" class="form-contato" enctype="multipart/form-data"><div class="inputs"><input type="text" name="nome" placeholder="'+formNome+'" value="" required><input type="email" name="email" placeholder="e-mail" value="" required><input type="text" name="telefone" placeholder="'+formTelefone+'" class="input-telefone" value=""></div><textarea name="mensagem" placeholder="'+formMensagem+'" required></textarea><button type="submit" class="btn-enviar">'+formEnviar+'</button></form>';
                        $("main.contato section.dados-form").append(htmlForm);

                        var htmlMapa = '<article class="endereco"></article><article class="mapa">'+data.contato.google_maps+'</article>';
                        $("main.contato section.endereco-mapa").append(htmlMapa);

                        if (langActive == "en") {
                            var htmlEndereco = '<p>'+data.contato.endereco_en+'</p><p>'+data.contato.bairro_en+'<span>∙</span>'+data.contato.cidade_en+'<span>∙</span>'+data.contato.pais_en+'</p><p>'+data.contato.cep+'</p>';
                        } else if (langActive == "es") {
                            var htmlEndereco = '<p>'+data.contato.endereco_es+'</p><p>'+data.contato.bairro_es+'<span>∙</span>'+data.contato.cidade_es+'<span>∙</span>'+data.contato.pais_es+'</p><p>'+data.contato.cep+'</p>';
                        } else {
                            var htmlEndereco = '<p>'+data.contato.endereco_pt+'</p><p>'+data.contato.bairro_pt+'<span>∙</span>'+data.contato.cidade_pt+'<span>∙</span>'+data.contato.pais_pt+'</p><p>'+data.contato.cep+'</p>';
                        }
                        $("main.contato section.endereco-mapa .endereco").append(htmlEndereco);

                        $("main").animate({ opacity: "1.0" }, 800);

                        window.history.pushState(
                            "",
                            "",
                            urlPrevia + "/contato"
                        );
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR, textStatus, errorThrown);
                    },
                });
            });
        };
    });
}
