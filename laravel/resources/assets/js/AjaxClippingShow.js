export default function AjaxClippingShow() {
    $(document).ready(function () {
        var urlPrevia = "";
        // var urlPrevia = "/previa-rogerioperez";

        // CLIPPING - ajax
        $("body").on("click", ".clipping-imagens", function (e) {
            e.preventDefault();
            getDadosClippingShow(this.href);
        });

        var getDadosClippingShow = function (url) {
            $("main").animate({ opacity: "0.0" }, 800, function () {
                $.ajax({
                    type: "GET",
                    url: url,
                    cache: false,
                    beforeSend: function () {
                        $("main").html("");
                        $("main").removeClass().addClass("clipping-show");
                    },
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);

                        var baseHtml = '<div class="center"></div>';
                        $("main.clipping-show").append(baseHtml);

                        data.imagens.forEach((imagem) => {
                            var html =
                                '<img src="' +
                                window.location.origin +
                                urlPrevia +
                                "/assets/img/clippings/imagens/" +
                                imagem.imagem +
                                '" class="img-clipping" alt="">';
                            $("main.clipping-show .center").append(html);
                        });

                        var htmlVoltar =
                            '<a href="' +
                            window.location.origin +
                            urlPrevia +
                            "/clipping/ajax" +
                            '" class="link-voltar-clipping">' +
                            btnVoltar +
                            "</a>";
                        $("main.clipping-show").append(htmlVoltar);

                        $("main").animate({ opacity: "1.0" }, 800);

                        window.history.pushState(
                            "",
                            "",
                            urlPrevia + "/clipping/" + data.id
                        );
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR, textStatus, errorThrown);
                    },
                });
            });
        };
    });
}
