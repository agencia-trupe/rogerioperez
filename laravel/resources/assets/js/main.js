import MobileToggle from "./MobileToggle";
import AjaxPerfil from "./AjaxPerfil";
import AjaxProjetos from "./AjaxProjetos";
import AjaxProjetosShow from "./AjaxProjetosShow";
import AjaxClipping from "./AjaxClipping";
import AjaxClippingShow from "./AjaxClippingShow";
import AjaxContato from "./AjaxContato";
import AjaxPoliticaDePrivacidade from "./AjaxPoliticaDePrivacidade";

MobileToggle();
AjaxPerfil();
AjaxProjetos();
AjaxProjetosShow();
AjaxClipping();
AjaxClippingShow();
AjaxContato();
AjaxPoliticaDePrivacidade();

$(document).ready(function () {
    var urlPrevia = "";
    // var urlPrevia = "/previa-rogerioperez";

    // SUBMENU LANG
    if ($(window).width() < 800) {
        $(".lang-active").click(function (e) {
            e.preventDefault();
            $(this).parent().children(".submenu-lang").css("display", "flex");
        });
    } else {
        $(".lang-active").mouseenter(function () {
            $(this).parent().children(".submenu-lang").css("display", "flex");
        });
        $(".submenu-lang").mouseleave(function () {
            $(this).css("display", "none");
        });
    }

    // MASK telefone
    $(".input-telefone").mask("(00) 000000000");

    // BANNERS HOME
    $(".banners").cycle({
        slides: ".banner",
        fx: "fade",
        speed: 2000,
        timeout: 6000,
    });

    // HOME para INTERNAS - efeito HEADER
    if (window.location.href == routeHome) {
        $("footer").css("display", "none");
        $("header").addClass("header-home");

        if ($(window).width() <= 800) {
            var calcTop = ($(window).height() - 80) / 2;
            $("header").animate({ top: calcTop }, 1000);
        } else {
            var calcTop = ($(window).height() - 100) / 2;
            $("header").animate({ top: calcTop }, 1000);
        }

        $("header .link-nav").click(function (e) {
            e.preventDefault();
            $("header").animate({ top: "0" }, 1000).removeClass("header-home");
            $("footer").css("display", "block");

            if ($(window).width() <= 800) {
                $("header nav").css("display", "none");
                $("header #mobile-toggle").removeClass("close");
            }

            $("body").css("overflow-y", "scroll");
        });

        $("body").css("overflow-y", "hidden");
    } else {
        if ($(window).width() <= 800) {
            $("header .link-nav").click(function (e) {
                $("header nav").css("display", "none");
                $("header #mobile-toggle").removeClass("close");
            });
        }
    }

    // PROJETOS - removendo margin 
    if ($(window).width() > 1200) {
        $("main.projetos .link-projeto:nth-child(5n)").css("margin-right", "0");
    } else if ($(window).width() < 800) {
        $("main.projetos .link-projeto:nth-child(2n)").css("margin-right", "0");
    } else {
        $("main.projetos .link-projeto:nth-child(4n)").css("margin-right", "0");
    }

    // PROJETOS - ver mais
    var itensProjetos = $("main.projetos .link-projeto");
    var spliceItensProjetos = 10; // 40 itens (no layout)
    if (itensProjetos.length <= spliceItensProjetos) {
        $(".link-ver-mais-projetos").hide();
    }
    var setDivProjetos = function () {
        var spliceItens = itensProjetos.splice(0, spliceItensProjetos);
        $("main.projetos .center").append(spliceItens);
        $(spliceItens).show();
        if (itensProjetos.length <= 0) {
            $(".link-ver-mais-projetos").hide();
        }
    };
    $("body").on("click", ".link-ver-mais-projetos", function (e) {
        e.preventDefault();
        setDivProjetos();
    });
    $("main.projetos .link-projeto").hide();
    setDivProjetos();

    // PROJETOS-SHOW - margin first element
    if ($(window).width() > 800) {
        $("main.projetos-show .link-imagem:eq(0)").css(
            "margin",
            "0 0 50px auto"
        );
    } else {
        $("main.projetos-show .link-imagem:eq(0)").css(
            "margin",
            "0 auto 20px auto"
        );
    }

    // PROJETOS-SHOW - fancybox // CLIPPING IMAGENS - fancybox
    $(".projetos-show .link-imagem").fancybox({
        padding: 0,
        prevEffect: "fade",
        nextEffect: "fade",
        closeBtn: false,
        openEffect: "elastic",
        openSpeed: "150",
        closeEffect: "elastic",
        closeSpeed: "150",
        keyboard: true,
        helpers: {
            title: {
                type: "outside",
                position: "top",
            },
            overlay: {
                css: {
                    background: "rgba(132, 134, 136, .88)",
                },
            },
        },
        mobile: {
            clickOutside: "close",
        },
        fitToView: false,
        autoSize: false,
        beforeShow: function () {
            this.maxWidth = "100%";
            this.maxHeight = "100%";
        },
    });

    // CLIPPING - ver mais
    var itensClipping = $("main.clipping .link-item-clipping");
    var spliceItensClipping = 10; // 40 itens (layout)
    if (itensClipping.length <= spliceItensClipping) {
        $(".link-ver-mais-clipping").hide();
    }
    var setDivClipping = function () {
        var spliceItens = itensClipping.splice(0, spliceItensClipping);
        $(spliceItens).show();
        if (itensClipping.length <= 0) {
            $(".link-ver-mais-clipping").hide();
        }
    };
    $("body").on("click", ".link-ver-mais-clipping", function (e) {
        e.preventDefault();
        setDivClipping();
    });
    $("main.clipping .link-item-clipping").hide();
    setDivClipping();

    // CLIPPINGS - grid
    $(".masonry-grid").masonryGrid({
        columns: 5,
    });
    $(".masonry-grid .masonry-grid-column:nth-child(5n)").css(
        "margin-right",
        "0"
    );

    // AVISO DE COOKIES
    $(".aviso-cookies").show();

    $(".aceitar-cookies").click(function () {
        var url = window.location.origin + urlPrevia + "/aceite-de-cookies";

        $.ajax({
            type: "POST",
            url: url,
            success: function (data, textStatus, jqXHR) {
                $(".aviso-cookies").hide();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            },
        });
    });
});
