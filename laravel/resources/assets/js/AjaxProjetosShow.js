export default function AjaxProjetosShow() {
    $(document).ready(function () {
        // var urlPrevia = "";
        var urlPrevia = "/previa-rogerioperez";

        $(document).scroll(function (e) {
            e.preventDefault();
            $(".btns-projeto-show").toggle($(document).scrollTop() !== 0);
        });

        // PROJETOS - ajax
        $("body").on("click", ".link-projeto", function (e) {
            e.preventDefault();
            getDadosProjetosShow(this.href);
        });

        $("body").on("click", ".link-proximo", function (e) {
            e.preventDefault();
            getDadosProjetosShow(this.href);
        });

        $("body").on("click", ".link-anterior", function (e) {
            e.preventDefault();
            getDadosProjetosShow(this.href);
        });

        $("body").on("click", ".link-topo", function (e) {
            e.preventDefault();
            $("html, body").animate({ scrollTop: $("body").offset().top }, 800);
        });

        var getDadosProjetosShow = function (url) {
            $("main").animate({ opacity: "0.0" }, 800, function () {
                $.ajax({
                    type: "GET",
                    url: url,
                    cache: false,
                    beforeSend: function () {
                        $("main").html("");
                        $("main").removeClass().addClass("projetos-show");
                    },
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);

                        var baseHtml = '<div class="center"></div>';
                        $("main.projetos-show").append(baseHtml);

                        var htmlDados = '<article class="dados"></article>';
                        $("main.projetos-show .center").append(htmlDados);

                        if (langActive == "en") {
                            var titulo =
                                '<p class="subtitulo">' +
                                projetoLang +
                                ':</p><p class="titulo">' +
                                data.projeto.titulo_en +
                                "</p>";
                            var localizacao =
                                '<p class="subtitulo">' +
                                localizacaoLang +
                                ':</p><p class="titulo">' +
                                data.projeto.localizacao_en +
                                "</p>";
                            var descritivo =
                                '<p class="subtitulo">' +
                                descritivoLang +
                                ':</p><div class="descritivo">' +
                                data.projeto.descritivo_en +
                                "</div>";
                        } else if (langActive == "es") {
                            var titulo =
                                '<p class="subtitulo">' +
                                projetoLang +
                                ':</p><p class="titulo">' +
                                data.projeto.titulo_es +
                                "</p>";
                            var localizacao =
                                '<p class="subtitulo">' +
                                localizacaoLang +
                                ':</p><p class="titulo">' +
                                data.projeto.localizacao_es +
                                "</p>";
                            var descritivo =
                                '<p class="subtitulo">' +
                                descritivoLang +
                                ':</p><div class="descritivo">' +
                                data.projeto.descritivo_es +
                                "</div>";
                        } else {
                            var titulo =
                                '<p class="subtitulo">' +
                                projetoLang +
                                ':</p><p class="titulo">' +
                                data.projeto.titulo_pt +
                                "</p>";
                            var localizacao =
                                '<p class="subtitulo">' +
                                localizacaoLang +
                                ':</p><p class="titulo">' +
                                data.projeto.localizacao_pt +
                                "</p>";
                            var descritivo =
                                '<p class="subtitulo">' +
                                descritivoLang +
                                ':</p><div class="descritivo">' +
                                data.projeto.descritivo_pt +
                                "</div>";
                        }
                        $("main.projetos-show .center .dados").append([
                            titulo,
                            localizacao,
                            descritivo,
                        ]);

                        data.imagens.forEach((imagem) => {
                            var htmlImagens =
                                '<a href="' +
                                window.location.origin +
                                urlPrevia +
                                "/assets/img/projetos/imagens/fancybox/" +
                                imagem.imagem +
                                '" class="link-imagem" rel="projetos"><img src="' +
                                window.location.origin +
                                urlPrevia +
                                "/assets/img/projetos/imagens/" +
                                imagem.imagem +
                                '" class="img-projeto" alt="' +
                                data.projeto.titulo_pt +
                                '"></a>';

                            $("main.projetos-show .center").append(htmlImagens);
                        });

                        var htmlBtns =
                            '<article class="btns-projeto-show" style="display:none;"><a href="" class="link-topo">' +
                            btnTopo +
                            '</a><a href="' +
                            window.location.origin +
                            urlPrevia +
                            "/projetos/" +
                            data.projeto.slug_pt +
                            "/proximo" +
                            '" class="link-proximo">' +
                            btnProximo +
                            '</a><a href="' +
                            window.location.origin +
                            urlPrevia +
                            "/projetos/" +
                            data.projeto.slug_pt +
                            "/anterior" +
                            '" class="link-anterior">' +
                            btnAnterior +
                            "</a></article>";
                        $("main.projetos-show .center").append(htmlBtns);

                        if ($(window).width() > 800) {
                            $("main.projetos-show .link-imagem:eq(0)").css(
                                "margin",
                                "0 0 50px auto"
                            );
                        } else {
                            $("main.projetos-show .link-imagem:eq(0)").css(
                                "margin",
                                "0 auto 20px auto"
                            );
                        }

                        $("main").animate({ opacity: "1.0" }, 800);

                        $("html, body").animate(
                            { scrollTop: $("body").offset().top },
                            1000
                        );

                        window.history.pushState(
                            "",
                            "",
                            urlPrevia + "/projetos/" + data.projeto.slug_pt
                        );
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR, textStatus, errorThrown);
                    },
                });
            });
        };
    });
}
