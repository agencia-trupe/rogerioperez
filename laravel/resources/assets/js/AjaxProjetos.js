export default function AjaxProjetos() {
    $(document).ready(function () {
        var urlPrevia = "";
        // var urlPrevia = "/previa-rogerioperez";

        // PROJETOS - ajax
        $("body").on("click", ".link-projetos", function (e) {
            e.preventDefault();
            $(".link-nav.active, .link-footer.active, .link-politica.active").removeClass("active");
            $(".link-projetos").addClass("active");
            getDadosProjetos(this.href);
        });

        var getDadosProjetos = function (url) {
            $("main").animate({ opacity: "0.0" }, 800, function () {
                $.ajax({
                    type: "GET",
                    url: url,
                    cache: false,
                    beforeSend: function () {
                        $("main").html("");
                        $("main").removeClass().addClass("projetos");
                    },
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);

                        var baseHtml = '<div class="center"></div>';
                        $("main.projetos").append(baseHtml);

                        data.projetos.forEach((projeto) => {
                            if(projeto.cor_texto == '#FFF') {
                                var html =
                                '<a href="' +
                                window.location.origin +
                                urlPrevia +
                                "/projetos/" +
                                projeto.slug_pt +
                                "/ajax" +
                                '" class="link-projeto" id="' +
                                projeto.id +
                                '" style="background-image: url(' +
                                window.location.origin +
                                urlPrevia +
                                "/assets/img/projetos/" +
                                projeto.capa +
                                ')"><p class="titulo titulo-projeto" style="color:' +
                                projeto.cor_texto +
                                '; text-shadow: -2px 1px 2px #000;"></p></a>';
                            } else {
                                var html =
                                '<a href="' +
                                window.location.origin +
                                urlPrevia +
                                "/projetos/" +
                                projeto.slug_pt +
                                "/ajax" +
                                '" class="link-projeto" id="' +
                                projeto.id +
                                '" style="background-image: url(' +
                                window.location.origin +
                                urlPrevia +
                                "/assets/img/projetos/" +
                                projeto.capa +
                                ')"><p class="titulo titulo-projeto" style="color:' +
                                projeto.cor_texto +
                                '; text-shadow: -2px 1px 2px #FFF;"></p></a>';
                            }
                            
                            $("main.projetos .center").append(html);

                            if (langActive == "en") {
                                var titulo = projeto.titulo_en;
                            } else if (langActive == "es") {
                                var titulo = projeto.titulo_es;
                            } else {
                                var titulo = projeto.titulo_pt;
                            }
                            $(
                                ".link-projeto#" +
                                    projeto.id +
                                    " .titulo-projeto"
                            ).append(titulo);
                        });

                        // PROJETOS - removendo margin 
                        if ($(window).width() > 1200) {
                            $("main.projetos .link-projeto:nth-child(5n)").css(
                                "margin-right",
                                "0"
                            );
                        } else if ($(window).width() < 800) {
                            $("main.projetos .link-projeto:nth-child(2n)").css(
                                "margin-right",
                                "0"
                            );
                        } else {
                            $("main.projetos .link-projeto:nth-child(4n)").css(
                                "margin-right",
                                "0"
                            );
                        }

                        var htmlVerMais =
                            '<a href="" class="link-ver-mais-projetos">' +
                            btnVerMais +
                            "</a>";
                        $("main.projetos").append(htmlVerMais);

                        // PROJETOS - ver mais
                        var itensProjetos = $("main.projetos .link-projeto");
                        var spliceItensProjetos = 10; // 40 itens (no layout)
                        if (itensProjetos.length <= spliceItensProjetos) {
                            $(".link-ver-mais-projetos").hide();
                        }
                        var setDivProjetos = function () {
                            var spliceItens = itensProjetos.splice(
                                0,
                                spliceItensProjetos
                            );
                            $("main.projetos .center").append(spliceItens);
                            $(spliceItens).show();
                            if (itensProjetos.length <= 0) {
                                $(".link-ver-mais-projetos").hide();
                            }
                        };
                        $("body").on(
                            "click",
                            ".link-ver-mais-projetos",
                            function (e) {
                                e.preventDefault();
                                setDivProjetos();
                            }
                        );
                        $("main.projetos .link-projeto").hide();
                        setDivProjetos();

                        $("main").animate({ opacity: "1.0" }, 800);

                        window.history.pushState(
                            "",
                            "",
                            urlPrevia + "/projetos"
                        );
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR, textStatus, errorThrown);
                    },
                });
            });
        };
    });
}
