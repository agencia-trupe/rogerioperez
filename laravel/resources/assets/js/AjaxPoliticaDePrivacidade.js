export default function AjaxPoliticaDePrivacidade() {
    $(document).ready(function () {
        var urlPrevia = "";
        // var urlPrevia = "/previa-rogerioperez";

        // POLÍTICA DE PRIVACIDADE - ajax
        $("body").on("click", ".link-politica", function (e) {
            e.preventDefault();
            $(".link-nav.active, .link-footer.active").removeClass("active");
            $(".link-politica").addClass("active");
            getDadosPolitica(this.href);
        });

        var getDadosPolitica = function (url) {
            $("main").animate({ opacity: "0.0" }, 800, function () {
                $.ajax({
                    type: "GET",
                    url: url,
                    cache: false,
                    beforeSend: function () {
                        $("main").html("");
                        $("main").removeClass().addClass("politica-de-privacidade");
                    },
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);

                        var baseHtml = '<div class="center"></div>';
                        $("main.politica-de-privacidade").append(baseHtml);

                        if (langActive == "en") {
                            var html = '<h2 class="titulo">'+politicaLang+'</h2><div class="texto">'+data.politica.texto_en+'</div>';
                        } else if (langActive == "es") {
                            var html = '<h2 class="titulo">'+politicaLang+'</h2><div class="texto">'+data.politica.texto_es+'</div>';
                        } else {
                            var html = '<h2 class="titulo">'+politicaLang+'</h2><div class="texto">'+data.politica.texto_pt+'</div>';
                        }
                        $("main.politica-de-privacidade .center").append(html);

                        $("main").animate({ opacity: "1.0" }, 800);

                        window.history.pushState(
                            "",
                            "",
                            urlPrevia + "/politica-de-privacidade"
                        );
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR, textStatus, errorThrown);
                    },
                });
            });
        };
    });
}
