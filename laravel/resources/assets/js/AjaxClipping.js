export default function AjaxClipping() {
    $(document).ready(function () {
        var urlPrevia = "";
        // var urlPrevia = "/previa-rogerioperez";

        $("body").on("click", ".link-clipping, .link-voltar-clipping", function (e) {
            e.preventDefault();
            $(".link-nav.active, .link-footer.active, .link-politica.active").removeClass("active");
            $(".link-clipping").addClass("active");
            getDadosClipping(this.href);
        });

        var getDadosClipping = function (url) {
            $("main").animate({ opacity: "0.0" }, 800, function () {
                $.ajax({
                    type: "GET",
                    url: url,
                    cache: false,
                    beforeSend: function () {
                        $("main").html("");
                        $("main").removeClass().addClass("clipping");
                    },
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);

                        var baseHtml =
                            '<div class="center masonry-grid"></div>';
                        $("main.clipping").append(baseHtml);

                        data.clippings.forEach((clipping) => {
                            if (clipping.tipo_id == 1) {
                                var html =
                                    '<a href="' +
                                    window.location.origin +
                                    urlPrevia +
                                    "/clipping/ajax/" +
                                    clipping.id +
                                    '" class="clipping-imagens link-item-clipping"><img src="' +
                                    window.location.origin +
                                    urlPrevia +
                                    "/assets/img/clippings/" +
                                    clipping.capa +
                                    '" class="img-capa" alt=""></a>';
                            }

                            if (clipping.tipo_id == 2) {
                                var html =
                                    '<a href="' +
                                    clipping.link +
                                    '" target="_blank" class="clipping-link link-item-clipping"><img src="' +
                                    window.location.origin +
                                    urlPrevia +
                                    "/assets/img/clippings/" +
                                    clipping.capa +
                                    '" class="img-capa" alt=""></a>';
                            }

                            if (clipping.tipo_id == 3) {
                                var html =
                                    '<a href="' +
                                    "https://www.youtube.com/watch?v=" +
                                    clipping.video +
                                    '" class="clipping-video link-item-clipping" data-fancybox-type="iframe"><iframe src="' +
                                    "https://www.youtube.com/embed/" +
                                    clipping.video +
                                    '"></iframe></a>';
                            }

                            if (clipping.tipo_id == 4) {
                                var html =
                                    '<a href="' +
                                    window.location.origin + urlPrevia + '/clipping/' + clipping.id + '/arquivo' +
                                    '" target="_blank" class="clipping-link link-item-clipping"><img src="' +
                                    window.location.origin +
                                    urlPrevia +
                                    "/assets/img/clippings/" +
                                    clipping.capa +
                                    '" class="img-capa" alt=""></a>';
                            }

                            $("main.clipping .center").append(html);
                        });

                        var htmlVerMais =
                            '<a href="" class="link-ver-mais-clipping">' +
                            btnVerMais +
                            "</a>";
                        $("main.clipping").append(htmlVerMais);

                        // CLIPPING - ver mais
                        var itensClipping = $("main.clipping .link-item-clipping");
                        var spliceItensClipping = 10; // 40 itens (layout)
                        if (itensClipping.length <= spliceItensClipping) {
                            $(".link-ver-mais-clipping").hide();
                        }
                        var setDivClipping = function () {
                            var spliceItens = itensClipping.splice(
                                0,
                                spliceItensClipping
                            );
                            $(spliceItens).show();
                            if (itensClipping.length <= 0) {
                                $(".link-ver-mais-clipping").hide();
                            }
                        };
                        $("body").on(
                            "click",
                            ".link-ver-mais-clipping",
                            function (e) {
                                e.preventDefault();
                                setDivClipping();
                            }
                        );
                        $("main.clipping .link-item-clipping").hide();
                        setDivClipping();

                        $(".masonry-grid").masonryGrid({
                            columns: 5,
                        });
                        // CLIPPING - removendo margin 5º elemento
                        $(
                            ".masonry-grid .masonry-grid-column:nth-child(5n)"
                        ).css("margin-right", "0");

                        $("main").animate({ opacity: "1.0" }, 800);

                        window.history.pushState(
                            "",
                            "",
                            urlPrevia + "/clipping"
                        );
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR, textStatus, errorThrown);
                    },
                });
            });
        };
    });
}
