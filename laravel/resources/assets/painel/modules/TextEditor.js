import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
// import RemoveFormat from "@ckeditor/ckeditor5-remove-format/src/removeformat";
// import Bold from "@ckeditor/ckeditor5-basic-styles/src/bold";
// import Italic from "@ckeditor/ckeditor5-basic-styles/src/italic";

export default function () {
    const $editors = document.querySelectorAll(".editor-padrao");

    if ($editors.length) {
        $editors.forEach(($editor) => {
            ClassicEditor.create($editor, {
                toolbar: ["bold", "italic", "bulletedList"],
            });
        });
    }
}
