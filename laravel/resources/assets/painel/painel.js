import Clipboard from "./modules/Clipboard.js";
import DataTables from "./modules/DataTables.js";
import DatePicker from "./modules/DatePicker.js";
import DeleteButton from "./modules/DeleteButton.js";
import ImagesUpload from "./modules/ImagesUpload.js";
import OrderImages from "./modules/OrderImages.js";
import OrderTable from "./modules/OrderTable.js";
import TextEditor from "./modules/TextEditor.js";
import MobileToggle from "../js/MobileToggle";

Clipboard();
DataTables();
DatePicker();
DeleteButton();
ImagesUpload();
OrderImages();
OrderTable();
TextEditor();
MobileToggle();

$(document).ready(function () {
    $(".select-tipos").change(function () {
        if ($(this).val() == 1) {
            $(".tipo-imagens").parent().css("display", "block");
            $(".tipo-imagens").attr("required", "");
            $(".tipo-link, .tipo-video, .tipo-pdf")
                .parent()
                .css("display", "none");
            $(".tipo-link, .tipo-video, .tipo-pdf").removeAttr("required");
        } else if ($(this).val() == 2) {
            $(".tipo-imagens, .tipo-link").parent().css("display", "block");
            $(".tipo-link").attr("required", "");
            $(".tipo-video, .tipo-pdf").parent().css("display", "none");
            $(".tipo-imagens, .tipo-video, .tipo-pdf").removeAttr("required");
        } else if ($(this).val() == 3) {
            $(".tipo-video").parent().css("display", "block");
            $(".tipo-video").attr("required", "");
            $(".tipo-imagens, .tipo-link, .tipo-pdf")
                .parent()
                .css("display", "none");
            $(".tipo-imagens, .tipo-link, .tipo-pdf").removeAttr("required");
        } else if ($(this).val() == 4) {
            $(".tipo-imagens, .tipo-pdf").parent().css("display", "block");
            $(".tipo-link, .tipo-video").parent().css("display", "none");
            $(".tipo-link, .tipo-video").removeAttr("required");
        } else {
            $(".tipo-imagens, .tipo-link, .tipo-video, .tipo-pdf")
                .parent()
                .css("display", "block");
            $(".tipo-imagens, .tipo-link, .tipo-video, .tipo-pdf").removeAttr("required");
        }
    });

    var tipoSelected = $(".select-tipos option:selected").val();
    console.log(tipoSelected);
    if (tipoSelected == 1) {
        $(".tipo-imagens").parent().css("display", "block");
        $(".tipo-imagens").attr("required", "");
        $(".tipo-link, .tipo-video, .tipo-pdf").parent().css("display", "none");
        $(".tipo-link, .tipo-video, .tipo-pdf").removeAttr("required");
    } else if (tipoSelected == 2) {
        $(".tipo-imagens, .tipo-link").parent().css("display", "block");
        $(".tipo-link").attr("required", "");
        $(".tipo-video, .tipo-pdf").parent().css("display", "none");
        $(".tipo-imagens, .tipo-video, .tipo-pdf").removeAttr("required");
    } else if (tipoSelected == 3) {
        $(".tipo-video").parent().css("display", "block");
        $(".tipo-video").attr("required", "");
        $(".tipo-imagens, .tipo-link, .tipo-pdf")
            .parent()
            .css("display", "none");
        $(".tipo-imagens, .tipo-link, .tipo-pdf").removeAttr("required");
    } else if (tipoSelected == 4) {
        $(".tipo-imagens, .tipo-pdf").parent().css("display", "block");
        $(".tipo-link, .tipo-video").parent().css("display", "none");
        $(".tipo-link, .tipo-video").removeAttr("required");
    } else {
        $(".tipo-imagens, .tipo-link, .tipo-video, .tipo-pdf")
            .parent()
            .css("display", "block");
        $(".tipo-imagens, .tipo-link, .tipo-video, .tipo-pdf").removeAttr("required");
    }
});
