<!doctype html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name') }} - Painel Administrativo</title>

    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/painel.css') }}">
</head>

<body class="painel-login">

    <div class="col-md-4 col-sm-6 col-xs-10 login">

        <h3 class="mb-5">
            {{ config('app.name') }}
            <small class="h6 lead mb-0" style="color:#b4bcc2;">Painel Administrativo</small>
        </h3>

        {!! Form::open(['route' => 'password.email']) !!}

        <!-- Email Address -->
        <div class="input-group">
            <span class="input-group-text">
                <i class="bi bi-person-fill"></i>
            </span>
            {!! Form::text('email', null, [
            'class' => 'form-control',
            'placeholder' => 'e-mail',
            'autofocus' => true,
            'required' => true
            ]) !!}
        </div>

        {!! Form::submit('SOLITICAR REDEFINIÇÃO DE SENHA', ['class' => 'btn btn-success col-12 mt-4', 'style' => 'height:45px']) !!}
        <a href="{{ route('login') }}" class="btn btn-secondary col-12 mt-1">Voltar</a>
        {!! Form::close() !!}

        <!-- Session Status -->
        <x-auth-session-status class="flash flash-sucesso mt-2" :status="session('status')" />

        <!-- Validation Errors -->
        <x-auth-validation-errors class="flash flash-erro mt-2" :errors="$errors" />
    </div>

</body>