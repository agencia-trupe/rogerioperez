@extends('frontend.layout.template')

@section('content')

<main class="perfil">

    <div class="center">

        <article class="left">
            <img src="{{ asset('assets/img/perfil/'.$perfil->imagem) }}" class="img-perfil">
            <div class="frase">{!! $perfil->{trans('database.frase')} !!}</div>
        </article>

        <article class="right">
            {!! $perfil->{trans('database.texto')} !!}
        </article>

    </div>

</main>

@endsection