@extends('frontend.layout.template')

@section('content')

<main class="politica-de-privacidade">
    <div class="center">
        <h2 class="titulo">{{ trans('frontend.geral.politica') }}</h2>
        <div class="texto">{!! $politica->{trans('database.texto')} !!}</div>
    </div>
</main>

@endsection