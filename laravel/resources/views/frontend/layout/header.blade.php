<header>

    <div class="center">

        <a href="{{ route('home') }}" class="link-home">
            <img src="{{ asset('assets/img/layout/marca-rogerioperez.svg') }}" alt="{{ $config->title }}" class="img-logo">
        </a>

        <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button>

        <nav>
            @include('frontend.layout.nav')
        </nav>

    </div>

</header>