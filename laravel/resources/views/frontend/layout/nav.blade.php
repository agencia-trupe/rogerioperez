<a href="{{ route('perfil.ajax') }}" class="link-nav link-perfil @if(Tools::routeIs('perfil*')) active @endif">{{ trans('frontend.geral.perfil') }}</a>
<a href="{{ route('projetos.ajax') }}" class="link-nav link-projetos @if(Tools::routeIs('projetos*')) active @endif">{{ trans('frontend.geral.projetos') }}</a>
<a href="{{ route('clipping.ajax') }}" class="link-nav link-clipping @if(Tools::routeIs('clipping*')) active @endif">{{ trans('frontend.geral.clipping') }}</a>
<a href="{{ route('contato.ajax') }}" class="link-nav link-contato @if(Tools::routeIs('contato*')) active @endif">{{ trans('frontend.geral.contato') }}</a>

<a href="{{ $contato->instagram }}" target="_blank" class="instagram"></a>

<div class="idiomas">
    @foreach([
    'pt' => '[pt]',
    'en' => '[en]',
    'es' => '[es]',
    ] as $key => $title)

    @if(app()->getLocale() == $key)
    <a href="{{ route('lang', $key) }}" class="lang-active">{{ $title }}</a>
    @endif
    @endforeach

    <div class="submenu-lang" style="display: none;">
        @foreach([
        'pt' => '[pt]',
        'en' => '[en]',
        'es' => '[es]',
        ] as $key => $title)

        @if(app()->getLocale() != $key)
        <a href="{{ route('lang', $key) }}" class="link-lang">{{ $title }}</a>
        @endif
        @endforeach
    </div>
</div>