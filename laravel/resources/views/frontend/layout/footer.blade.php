<footer>

    <div class="center">

        <a href="{{ route('home') }}" class="link-home">
            <img src="{{ asset('assets/img/layout/marca-rogerioperez-rodape.svg') }}" alt="{{ $config->title }}" class="img-logo">
        </a>

        <article class="informacoes">
            <div class="contatos">
                <div class="telefones">
                    @php
                    $telefone = str_replace(" ", "", $contato->telefone);
                    $celular = str_replace(" ", "", $contato->celular);
                    @endphp
                    <a href="tel:{{ $telefone }}" class="link-telefone">{{ $contato->telefone }}</a>
                    <a href="https://api.whatsapp.com/send?phone={{ $celular }}" class="link-wpp" target="_blank">{{ $contato->celular }}</a>
                </div>
                <a href="{{ $contato->instagram }}" target="_blank" class="instagram">@rogerioperezarquitetura</a>
            </div>
            <div class="endereco">
                <p>{{ $contato->{trans('database.endereco')} }}</p>
                <span>∙</span>
                <p>{{ $contato->{trans('database.bairro')} }}</p>
                <span>∙</span>
                <p>{{ $contato->cep }}</p>
                <span>∙</span>
                <p>{{ $contato->{trans('database.cidade')} }}</p>
                <span>∙</span>
                <p>{{ $contato->{trans('database.pais')} }}</p>
            </div>
            <a href="{{ route('politica-de-privacidade.ajax') }}" class="link-politica @if(Tools::routeIs('politica-de-privacidade*')) active @endif">{{ trans('frontend.geral.politica') }}</a>
        </article>

        <article class="menu">
            <a href="" class="link-footer">HOME</a>
            <a href="{{ route('perfil.ajax') }}" class="link-footer link-perfil @if(Tools::routeIs('perfil*')) active @endif">{{ trans('frontend.geral.perfil') }}</a>
            <a href="{{ route('projetos.ajax') }}" class="link-footer link-projetos @if(Tools::routeIs('projetos*')) active @endif">{{ trans('frontend.geral.projetos') }}</a>
            <a href="{{ route('clipping.ajax') }}" class="link-footer link-clipping @if(Tools::routeIs('clipping*')) active @endif">{{ trans('frontend.geral.clipping') }}</a>
            <a href="{{ route('contato.ajax') }}" class="link-footer link-contato @if(Tools::routeIs('contato*')) active @endif">{{ trans('frontend.geral.contato') }}</a>
        </article>

        <article class="copyright">
            <p class="dados"> © {{ date('Y') }} {{ config('app.name') }}</p>
            <p class="direitos">{{ trans('frontend.direitos') }}</p>
            <a href="https://www.trupe.net" target="_blank" class="link-trupe">{{ trans('frontend.criacao') }}</a>
            <a href="https://www.trupe.net" target="_blank" class="link-trupe">Trupe Agência Criativa</a>
        </article>

    </div>

</footer>