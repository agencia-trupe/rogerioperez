@extends('frontend.layout.template')

@section('content')

<main class="clipping-show">

    <div class="center">
        @foreach($imagens as $imagem)
        <img src="{{ asset('assets/img/clippings/imagens/'.$imagem->imagem) }}" class="img-clipping" alt="">
        @endforeach
    </div>

    <a href="{{ route('clipping.ajax') }}" class="link-voltar-clipping">{{ trans('frontend.clipping.voltar') }}</a>

</main>

@endsection