@extends('frontend.layout.template')

@section('content')

<main class="projetos-show">

    <div class="center">

        <article class="dados">
            <p class="subtitulo">{{ trans('frontend.projetos.projeto') }}:</p>
            <p class="titulo">{{ $projeto->{trans('database.titulo')} }}</p>

            <p class="subtitulo">{{ trans('frontend.projetos.localizacao') }}:</p>
            <p class="titulo">{{ $projeto->{trans('database.localizacao')} }}</p>

            <p class="subtitulo">{{ trans('frontend.projetos.descritivo') }}:</p>
            <div class="descritivo">{!! $projeto->{trans('database.descritivo')} !!}</div>
        </article>

        @foreach($imagens as $imagem)
        <a href="{{ asset('assets/img/projetos/imagens/fancybox/'.$imagem->imagem) }}" class="link-imagem" rel="projetos">
            <img src="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" class="img-projeto" alt="{{ $projeto->{trans('database.titulo')} }}">
        </a>
        @endforeach

        <article class="btns-projeto-show" style="display: none;">
            <a href="" class="link-topo">{{ trans('frontend.projetos.topo') }}</a>
            <a href="{{ route('projetos-proximo.ajax', $projeto->slug_pt) }}" class="link-proximo">{{ trans('frontend.projetos.proximo') }}</a>
            <a href="{{ route('projetos-anterior.ajax', $projeto->slug_pt) }}" class="link-anterior">{{ trans('frontend.projetos.anterior') }}</a>
        </article>

    </div>

</main>

@endsection