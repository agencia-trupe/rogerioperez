@extends('frontend.layout.template')

@section('content')

<main class="clipping">

    <div class="center masonry-grid">

        @foreach($clippings as $clipping)

        @if($clipping->tipo_id == 1)
        <a href="{{ route('clipping-show.ajax', $clipping->id) }}" class="clipping-imagens link-item-clipping">
            <img src="{{ asset('assets/img/clippings/'.$clipping->capa) }}" class="img-capa" alt="">
        </a>
        @endif

        @if($clipping->tipo_id == 2)
        <a href="{{ $clipping->link }}" target="_blank" class="clipping-link link-item-clipping">
            <img src="{{ asset('assets/img/clippings/'.$clipping->capa) }}" class="img-capa" alt="">
        </a>
        @endif

        @if($clipping->tipo_id == 3)
        @php
        $linkVideo = "https://www.youtube.com/embed/".$clipping->video;
        $linkYoutube = "https://www.youtube.com/watch?v=".$clipping->video;
        @endphp
        <a href="{{ $linkYoutube }}" class="clipping-video link-item-clipping" data-fancybox-type="iframe">
            <iframe src="{{ $linkVideo }}"></iframe>
        </a>
        @endif

        @if($clipping->tipo_id == 4)
        <a href="{{ route('clipping.arquivo', $clipping->id) }}" target="_blank" class="clipping-link link-item-clipping">
            <img src="{{ asset('assets/img/clippings/'.$clipping->capa) }}" class="img-capa" alt="">
        </a>
        @endif

        @endforeach

    </div>

    <a href="" class="link-ver-mais-clipping">{{ trans('frontend.clipping.ver-mais') }}</a>

</main>

@endsection