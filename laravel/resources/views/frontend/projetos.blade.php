@extends('frontend.layout.template')

@section('content')

<main class="projetos">

    <div class="center">

        @foreach($projetos as $projeto)
        <a href="{{ route('projetos-show.ajax', $projeto->slug_pt) }}" class="link-projeto" style="background-image: url({{ asset('assets/img/projetos/'.$projeto->capa) }})">
            @if($projeto->cor_texto == '#FFF')
            <p class="titulo" style="color: {{$projeto->cor_texto}};text-shadow: -2px 1px 2px #000;">{{ $projeto->{trans('database.titulo')} }}</p>
            @else
            <p class="titulo" style="color: {{$projeto->cor_texto}};text-shadow: -2px 1px 2px #FFF;">{{ $projeto->{trans('database.titulo')} }}</p>
            @endif
        </a>
        @endforeach

    </div>

    <a href="" class="link-ver-mais-projetos">{{ trans('frontend.clipping.ver-mais') }}</a>

</main>

@endsection