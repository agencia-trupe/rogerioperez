@extends('frontend.layout.template')

@section('content')

<main class="contato">

    <div class="center">

        <section class="dados-form">

            <article class="dados">
                @php
                $telefone = str_replace(" ", "", $contato->telefone);
                $celular = str_replace(" ", "", $contato->celular);
                @endphp
                <a href="tel:{{ $telefone }}" class="link-telefone">{{ $contato->telefone }}</a>
                <a href="https://api.whatsapp.com/send?phone={{ $celular }}" class="link-wpp" target="_blank">{{ $contato->celular }}</a>
                <a href="{{ $contato->instagram }}" target="_blank" class="link-instagram">@rogerioperezarquitetura</a>
            </article>

            <form action="{{ route('contato.post') }}" method="POST" class="form-contato" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="inputs">
                    <input type="text" name="nome" placeholder="{{ trans('frontend.contato.nome') }}" value="{{ old('nome') }}" required>
                    <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                    <input type="text" name="telefone" placeholder="{{ trans('frontend.contato.telefone') }}" class="input-telefone" value="{{ old('telefone') }}">
                </div>
                <textarea name="mensagem" placeholder="{{ trans('frontend.contato.mensagem') }}" required>{{ old('mensagem') }}</textarea>
                <button type="submit" class="btn-enviar">{{ trans('frontend.contato.enviar') }}</button>
            </form>

            @if($errors->any())
            <div class="flash flash-erro">
                @foreach($errors->all() as $error)
                {!! $error !!}<br>
                @endforeach
            </div>
            @endif

            @if(session('enviado'))
            <div class="flash flash-sucesso">
                <p>{{ trans('frontend.contato.msg-sucesso') }}</p>
            </div>
            @endif

        </section>

        <section class="endereco-mapa">

            <article class="endereco">
                <p>{{ $contato->{trans('database.endereco')} }}</p>
                <p>{{ $contato->{trans('database.bairro')} }}<span>∙</span>{{ $contato->{trans('database.cidade')} }}<span>∙</span>{{ $contato->{trans('database.pais')} }}</p>
                <p>{{ $contato->cep }}</p>
            </article>

            <article class="mapa">
                {!! $contato->google_maps !!}
            </article>

        </section>

    </div>

</main>

@endsection