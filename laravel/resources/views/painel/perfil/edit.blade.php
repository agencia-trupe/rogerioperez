@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">PERFIL</h2>
</legend>

{!! Form::model($perfil, [
'route' => ['perfil.update', $perfil->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.perfil.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection