@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($perfil->imagem)
    <img src="{{ url('assets/img/perfil/'.$perfil->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_pt', 'Frase (PT)') !!}
        {!! Form::textarea('frase_pt', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_en', 'Frase (EN)') !!}
        {!! Form::textarea('frase_en', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_es', 'Frase (ES)') !!}
        {!! Form::textarea('frase_es', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
</div>

<div class="mb-3 col-12">
    {!! Form::label('texto_pt', 'Texto (PT)') !!}
    {!! Form::textarea('texto_pt', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('texto_en', 'Texto (EN)') !!}
    {!! Form::textarea('texto_en', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('texto_es', 'Texto (ES)') !!}
    {!! Form::textarea('texto_es', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('perfil.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>