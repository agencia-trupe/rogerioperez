@include('painel.layout.flash')

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('capa', 'Capa') !!}
    @if($submitText == 'Alterar')
    @if($projeto->capa)
    <img src="{{ url('assets/img/projetos/'.$projeto->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('cor_texto', 'Cor do Título') !!}
    {!! Form::select('cor_texto', $cores , old('cor_texto'), ['class' => 'form-control']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_pt', 'Título (PT)') !!}
        {!! Form::text('titulo_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_en', 'Título (EN)') !!}
        {!! Form::text('titulo_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_es', 'Título (ES)') !!}
        {!! Form::text('titulo_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('localizacao_pt', 'Localização (PT)') !!}
        {!! Form::text('localizacao_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('localizacao_en', 'Localização (EN)') !!}
        {!! Form::text('localizacao_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('localizacao_es', 'Localização (ES)') !!}
        {!! Form::text('localizacao_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="mb-3 col-12">
    {!! Form::label('descritivo_pt', 'Descritivo (PT)') !!}
    {!! Form::textarea('descritivo_pt', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('descritivo_en', 'Descritivo (EN)') !!}
    {!! Form::textarea('descritivo_en', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('descritivo_es', 'Descritivo (ES)') !!}
    {!! Form::textarea('descritivo_es', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('projetos.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>