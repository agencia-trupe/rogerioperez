@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>PROJETOS |</small> Editar Projeto</h2>
</legend>

{!! Form::model($projeto, [
'route' => ['projetos.update', $projeto->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.projetos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection