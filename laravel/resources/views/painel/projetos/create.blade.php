@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>PROJETOS |</small> Adicionar Projeto</h2>
</legend>

{!! Form::open(['route' => 'projetos.store', 'files' => true]) !!}

@include('painel.projetos.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection