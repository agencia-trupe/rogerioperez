@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">PROJETOS</h2>

    <a href="{{ route('projetos.create') }}" class="btn btn-success btn-sm">
        <i class="bi bi-plus-circle me-2 mb-1"></i>
        Adicionar Projeto
    </a>
</legend>


@if(!count($projetos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="projetos">
        <thead>
            <tr>
                <th scope="col">Ordenar</th>
                <th scope="col">Capa</th>
                <th scope="col">Título [PT]</th>
                <th scope="col">Cor do Texto</th>
                <th scope="col">Gerenciar</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($projetos as $projeto)
            <tr id="{{ $projeto->id }}">
                <td>
                    <a href="#" class="btn btn-dark btn-sm btn-move">
                        <i class="bi bi-arrows-move"></i>
                    </a>
                </td>
                <td>
                    <img src="{{ asset('assets/img/projetos/'.$projeto->capa) }}" style="width: auto; max-width:100px;" alt="">
                </td>
                <td>{{ $projeto->titulo_pt }}</td>
                <td>{{ $cores[$projeto->cor_texto] }}</td>
                <td>
                    <a href="{{ route('projetos.imagens.index', $projeto->id) }}" class="btn btn-secondary btn-gerenciar btn-sm">
                        <i class="bi bi-images me-2"></i>IMAGENS
                    </a>
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['projetos.destroy', $projeto->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('projetos.edit', $projeto->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection