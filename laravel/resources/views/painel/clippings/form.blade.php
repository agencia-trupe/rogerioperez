@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('tipo_id', 'Tipo') !!}
    {!! Form::select('tipo_id', $tipos , old('tipo_id'), ['class' => 'form-control select-tipos']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('capa', 'Capa') !!}
    @if($submitText == 'Alterar')
    @if($clipping->capa)
    <img src="{{ url('assets/img/clippings/'.$clipping->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    @endif
    {!! Form::file('capa', ['class' => 'form-control tipo-imagens']) !!}
</div>

<div class="mb-3 col-12 col-md-12" style="display: none;">
    {!! Form::label('link', 'Link') !!}
    {!! Form::text('link', null, ['class' => 'form-control input-text tipo-link']) !!}
</div>

<div class="mb-3 col-12 col-md-12" style="display: none;">
    {!! Form::label('video', 'Vídeo') !!}
    {!! Form::text('video', null, ['class' => 'form-control input-text tipo-video']) !!}
    <p style="color:red;margin:5px 0 0 0;font-style:italic;">Incluir apenas o código do vídeo, a parte após "v=". Exemplo: https://www.youtube.com/watch?v=<strong>kUtybP_IDKg</strong></p>
</div>

<div class="mb-3 col-12 col-md-12" style="display: none;">
    {!! Form::label('arquivo', 'Arquivo') !!}
    @if($submitText == 'Alterar')
    @if($clipping->arquivo)
    <a href="{{ route('clipping.arquivo', $clipping->id) }}" target="_blank" style="display:block; margin-bottom: 10px; max-width: 100%;">{{ $clipping->arquivo }}</a>
    @endif
    @endif
    {!! Form::file('arquivo', ['class' => 'form-control tipo-pdf']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('clippings.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>