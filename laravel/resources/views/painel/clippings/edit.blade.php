@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CLIPPINGS |</small> Editar Clipping</h2>
</legend>

{!! Form::model($clipping, [
'route' => ['clippings.update', $clipping->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.clippings.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection