@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<a href="{{ route('clippings.index') }}" title="Voltar para Clippings" class="btn btn-secondary mb-3 col-2">
    &larr; Voltar para Clippings</a>

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">Imagens</h2>

    {!! Form::open(['route' => ['clippings.imagens.store', $clipping->id], 'files' => true, 'class' => 'pull-right']) !!}
    <div class="btn-group btn-group-sm">
        <span class="btn btn-success" style="position:relative;overflow:hidden">
            <i class="bi bi-plus-circle me-2 mb-1"></i>Adicionar Imagens
            <input id="images-upload" type="file" name="imagem" id="imagem" multiple style="position:absolute;top:0;right:0;opacity:0;font-size:200px;cursor:pointer;">
        </span>

        <a href="{{ route('clippings.imagens.clear', $clipping->id) }}" class="btn btn-danger btn-sm btn-delete btn-delete-link btn-delete-multiple">
            <i class="bi bi-trash-fill me-2"></i>Limpar
        </a>
    </div>
    {!! Form::close() !!}
</legend>

<div class="progress progress-striped active">
    <div class="progress-bar" style="width: 0"></div>
</div>

<div class="alert alert-block alert-danger errors" style="display:none"></div>

<div class="d-flex flex-row align-items-center justify-content-between mt-2">
    <div class="alert alert-info" style="display:inline-block;padding:10px 15px;">
        <small>
            <i class="bi bi-arrows-move me-2"></i>
            Clique e arraste as imagens para ordená-las.
        </small>
    </div>
</div>

<div id="imagens" data-table="clippings_imagens" style="width: 100%; display: flex; flex-wrap: wrap;">
    @if(!count($imagens))
    <div class="alert alert-warning no-images" style="width: 100%;" role="alert">Nenhuma imagem cadastrada.</div>
    @else

    @foreach($imagens as $imagem)
    @include('painel.clippings.imagens.imagem')
    @endforeach

    @endif
</div>

@endsection