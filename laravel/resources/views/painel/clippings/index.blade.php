@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">CLIPPINGS</h2>

    <a href="{{ route('clippings.create') }}" class="btn btn-success btn-sm">
        <i class="bi bi-plus-circle me-2 mb-1"></i>
        Adicionar Clipping
    </a>
</legend>

<div class="mt-3 mb-5">
    <h4>Tipos:</h4>
    <div class="row ps-2">
        @foreach($tipos as $tipo)
        <a href="{{ route('clippings.index', ['tipo' => $tipo->id]) }}" class="btn btn-secondary col-12 col-md-2 mx-1 {{ (isset($_GET['tipo']) && $_GET['tipo'] == $tipo->id) ? 'active' : '' }}">{{ $tipo->titulo }}</a>
        @endforeach
    </div>
</div>

@if(!count($clippings))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="clippings">
        <thead>
            <tr>
                <th scope="col">Ordenar</th>
                <th scope="col">Capa</th>
                <th scope="col">Tipo</th>
                <th scope="col">Conteúdo</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($clippings as $clipping)
            <tr id="{{ $clipping->id }}">
                <td>
                    <a href="#" class="btn btn-dark btn-sm btn-move">
                        <i class="bi bi-arrows-move"></i>
                    </a>
                </td>
                <td>
                    @if(isset($clipping->capa))
                    <img src="{{ asset('assets/img/clippings/'.$clipping->capa) }}" style="width: auto; max-width:100px;" alt="">
                    @else
                    Sem capa
                    @endif
                </td>
                <td>{{ $clipping->tipo }}</td>
                <td>
                    @if($clipping->tipo == "Imagens")
                    <a href="{{ route('clippings.imagens.index', $clipping->id) }}" class="btn btn-secondary btn-gerenciar btn-sm">
                        <i class="bi bi-images me-2"></i>IMAGENS
                    </a>
                    @elseif($clipping->tipo == "Link")
                    <a href="{{ $clipping->link }}">
                        {{ $clipping->link }}
                    </a>
                    @elseif($clipping->tipo == "PDF")
                    <a href="{{ route('clipping.arquivo', $clipping->id) }}" target="_blank">{{ $clipping->arquivo }}</a>
                    @else
                    <iframe src="{{ 'https://www.youtube.com/embed/'.$clipping->video }}" frameborder="0" width="120px" height="60px"></iframe>
                    @endif
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['clippings.destroy', $clipping->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('clippings.edit', $clipping->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection