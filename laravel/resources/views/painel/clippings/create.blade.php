@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CLIPPINGS |</small> Adicionar Clipping</h2>
</legend>

{!! Form::open(['route' => 'clippings.store', 'files' => true]) !!}

@include('painel.clippings.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection